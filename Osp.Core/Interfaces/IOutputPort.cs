namespace Osp.Core.Interfaces
{
    /// <summary>
    /// UseCase sends output through this output port to a controller
    /// </summary>
    /// <typeparam name="TUseCaseResponse"></typeparam>
    public interface IOutputPort<in TUseCaseResponse>
    {
        void Handle(TUseCaseResponse response);
    }
}