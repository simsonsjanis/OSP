﻿using System.Threading.Tasks;
using Osp.Core.Domain.Entities;
using Osp.Core.Dto.GatewayResponses.Repositories;

namespace Osp.Core.Interfaces.Gateways.Repositories
{
    public interface IProductRepository
    {
        Task<CreateProductDbResponse> Create(Product product);
    }
}
