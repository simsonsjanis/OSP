using Osp.Core.Dto.UseCaseRequests;
using Osp.Core.Dto.UseCaseResponses;

namespace Osp.Core.Interfaces.UseCases
{
    public interface ICreateProductUseCase : IUseCaseRequestHandler<CreateProductRequest, CreateProductResponse>
    {
    }
}