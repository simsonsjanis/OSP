using System.Threading.Tasks;

namespace Osp.Core.Interfaces
{
    /// <summary>
    /// defines the shape of all use case classes. generic interface.
    /// </summary>
    public interface IUseCaseRequestHandler<in TUseCaseRequest, out TUseCaseResponse> where TUseCaseRequest : IUseCaseRequest<TUseCaseResponse>
    {
        /// <summary>
        /// Request handler for all use cases, Request and Response must be defined by the specific UseCase class.
        /// Here we're only setting up the common way all use cases must handle requests.
        /// </summary>
        /// <param name="message">UseCase defined request sent in from outside layer</param>
        /// <param name="outputPort">UseCase defined output port sent in from outside layer. UseCase will send the response to this port.</param>
        /// <returns></returns>
        Task<bool> Handle(TUseCaseRequest message, IOutputPort<TUseCaseResponse> outputPort);
    }
}