﻿# Product management system
## Requirements

### Actions:
* Create new product
* Get existing product
* Modify existing product

### Rules:
* Product consists of name, quantity, description, date added, date updated
* Product name must be unique


