﻿using System;
using System.Collections.Generic;
using System.Text;
using Osp.Core.Dto.UseCaseResponses;
using Osp.Core.Interfaces;

namespace Osp.Core.Dto.UseCaseRequests
{
    public class CreateProductRequest : IUseCaseRequest<CreateProductResponse>
    {
        public CreateProductRequest(string name, int quantity, string description)
        {
            Name = name;
            Quantity = quantity;
            Description = description;
        }

        public string Name { get; }
        public int Quantity { get; }
        public string Description { get; }
    }
}
