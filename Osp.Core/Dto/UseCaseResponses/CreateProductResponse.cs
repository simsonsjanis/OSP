﻿using System;
using System.Collections.Generic;
using System.Text;
using Osp.Core.Interfaces;

namespace Osp.Core.Dto.UseCaseResponses
{
    public class CreateProductResponse : UseCaseResponseMessage
    {
        public int Id { get; }
        public IEnumerable<string> Errors { get; }

        public CreateProductResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }

        public CreateProductResponse(int id, bool success = false, string message = null) : base(success, message)
        {
            Id = id;
        }



    }
}
