﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Osp.Core.Dto.GatewayResponses.Repositories
{
    public sealed class CreateProductDbResponse : BaseGatewayResponse
    {
        public int Id { get; }

        public CreateProductDbResponse(int id, bool success = false, IEnumerable<Error> errors = default(IEnumerable<Error>)) : base(success, errors)
        {
            Id = id;
        }
    }
}
