﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Osp.Core.Domain.Entities
{
    public class Product
    {
        public Product(string name, int quantity, string description)
        {
            Name = name;
            Quantity = quantity;
            Description = description;
        }

        public string Name { get; }
        public int Quantity { get; }
        public string Description { get; }
    }
}
