using System.Linq;
using System.Threading.Tasks;
using Osp.Core.Domain.Entities;
using Osp.Core.Dto.GatewayResponses.Repositories;
using Osp.Core.Dto.UseCaseRequests;
using Osp.Core.Dto.UseCaseResponses;
using Osp.Core.Interfaces;
using Osp.Core.Interfaces.Gateways.Repositories;
using Osp.Core.Interfaces.UseCases;

namespace Osp.Core.UseCases
{
    public class CreateProductUseCase : ICreateProductUseCase
    {
        private readonly IProductRepository productRepository;

        public CreateProductUseCase(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }


        public async Task<bool> Handle(CreateProductRequest message, IOutputPort<CreateProductResponse> outputPort)
        {
            // send a reques to repository for creating new product
            CreateProductDbResponse response = await productRepository.Create(new Product(message.Name, message.Quantity, message.Description));

            // return whether repo response was successful or not
            outputPort.Handle(response.Success ? new CreateProductResponse(response.Id, true) : new CreateProductResponse(response.Errors.Select(e => e.Description)));
            return response.Success;
        }
    }
}