using System;
using System.Threading.Tasks;
using Moq;
using Xunit;
using Osp.Core;
using Osp.Core.Domain.Entities;
using Osp.Core.Dto.GatewayResponses.Repositories;
using Osp.Core.Dto.UseCaseRequests;
using Osp.Core.Dto.UseCaseResponses;
using Osp.Core.Interfaces;
using Osp.Core.Interfaces.Gateways.Repositories;
using Osp.Core.UseCases;

namespace Osp.Core.Tests
{
    public class CreateProductUseCaseTests
    {
        [Fact]
        public async void WhenProductCreated_ThenProductReturned()
        {
            /* arrange */
            // mocked product repo for storing products 
            var productRepoMock = new Mock<IProductRepository>();
            productRepoMock
                .Setup(repo => repo.Create(It.IsAny<Product>()))
                .Returns(Task.FromResult(new CreateProductDbResponse(0, true)));

            // output port is used for passing data from the UseCase to a Presenter,
            // so it can be prepared for final delivery to the UI or API
            var outputPort = new Mock<IOutputPort<CreateProductResponse>>();
            outputPort.Setup(x => x.Handle(It.IsAny<CreateProductResponse>()));

            // UseCase for creating products, the actual meat of the test
            var useCase = new CreateProductUseCase(productRepoMock.Object);

            var createProductRequest = new CreateProductRequest(name: "Plasma Shield", quantity: 5,
                description: "Personal protection shield");

            /* act */
            var response = await useCase.Handle(createProductRequest, outputPort.Object);

            /* assert */
            Assert.True(response);
        }


    }
}
